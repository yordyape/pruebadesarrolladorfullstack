import Router from "@koa/router";
import { addMovie, getAllM } from '../resources/movies'

const router = new Router({
    prefix: '/movies',
});

router.post('/', addMovie);
router.get('/', getAllM);

export = router;

// import  { allMovies } from "../resources/movies";

// export const router = new Router();

// router.get('/movies', allMovies );
// router.get('/pro', async (ctx) => {
//     ctx.body = 'hello world' ;
   
// });

// // router.get('/gv', async (ctx, next) => {
// //     movies.allMovies();
// //     await next();
// // });



import { Context } from 'koa';
import MoviesModel from '../models/movies';
import { generateToken } from '../function/create_token';
//import { encrypt } from '../function/secure_data';
//import R from 'ramda';



const addMovie = async (ctx: Context) => {
    console.log(ctx.request);
    //const movie = ctx.request.body.movie;
    const { movie, author, year } = ctx.request.body;

    console.log(movie, author, year);

    if(!movie)
        ctx.throw(400, {
            message: "Please enter a movie",
        });
    
    if(!author)
        ctx.throw(400, {
            message: "Please enter a author",
        })
    
    if(!year)
        ctx.throw(400, {
            message: "Please enter a year",
        })

    const existingMovie = await MoviesModel.findOne({
        movie,
    });

    if(existingMovie)
        ctx.throw(404, {
            message: 'Movie already exist, please try different movie.',
        })


    const moviefinal = await MoviesModel.create({
        movie,
        author,
        year,
    });

    const token = generateToken(moviefinal);
    ctx.status = 201;
    ctx.body = { token };
};


const getAllM = async (ctx: Context) => {
    console.log("hola");
    ctx.body = await MoviesModel.find({});
    // return await MoviesModel.find({});
  }


  const findReplaceMovies = async (ctx: Context) => {
    const { movie, find, replace} = ctx.body as MoviesModel;
    const query = { 'name': movie };
    const movies = mongoose.model('movies', movieSchema);

    const record = await movies.findOne({ 'name': movie });
    record.plot = record.plot.replace(find, replace);
    movies.findByIdAndUpdate(record.name, { 'plot': record.plot });
}


export { addMovie, getAllM };